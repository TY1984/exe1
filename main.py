FIRST_DIGIT = '0'
LAST_DIGIT = '9'
FIRST_CHARACTER = 'A'
LAST_CHARACTER = 'Z'


def validate_base(num_as_string, base):
    formatted_string = build_base(base)
    for character in num_as_string:
        if character not in formatted_string:
            print("The number {} is not a legal number in base {}".format(num_as_string, base))
            return False
    print("{} is a valid number in base {}".format(num_as_string, base))
    return True


def convert_to_dec(string, src_base):
    formatted_string = build_base(src_base)
    index = len(string) - 1
    decimal_value = 0
    for character in string:
        decimal_value += formatted_string.index(character) * (src_base ** index)
        index -= 1
    return decimal_value


def convert_to_dst(decimal,num_as_string, dst_base):
    formatted_string = build_base(dst_base)
    converted_num = ""
    while decimal > 0:
        number = decimal % dst_base
        decimal = int(decimal / dst_base)
        converted_num += formatted_string[number]
    print("The representation of {} in base {} is : {}".format(num_as_string, dst_base, converted_num[::-1]))


def convert(num_as_string, src_base, dst_base):

    # if src_base > 36 or src_base < 2:
    #     print("Source base {} is not a legal base".format(src_base))
    #     return False
    num_as_string = num_as_string.upper()
    if not (validate_base(num_as_string, src_base)):
        return False
    convert_to_dst(convert_to_dec(num_as_string, src_base),num_as_string, dst_base)
    return True


def build_base(base):
    clean_array = [chr(i) for i in range(ord(FIRST_DIGIT), ord(LAST_CHARACTER) + 1) if
                   ord(chr(i)) < ord(':') or ord(chr(i)) > ord('@')]
    return [clean_array[i] for i in range(base)]


def check_bases_validity(base):
    if not base.isdigit() or int(base) > 36 or int(base) < 2:
        print("Base should be in a digit form and in the range of 2 to 36 !")
        return False
    return True


def main():
    print("**********************************************")
    string = input("Please enter a string to convert: \n")

    while True:
        src_base = input("Please enter the source base: \n")
        if not check_bases_validity(src_base):
            continue
        src_base = int(src_base)
        break
    while True:
        dst_base = input("Please enter the destination base: \n")
        if not check_bases_validity(dst_base):
            continue
        dst_base = int(dst_base)
        break
    print("**********************************************")

    convert(string, src_base, dst_base)


if __name__ == '__main__':
    main()
