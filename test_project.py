import pytest
from main import convert_to_dst, convert_to_dec, validate_num_base, build_base


def test_convert_to_dec():
    assert convert_to_dec('4B2', 16) == 1202
    assert convert_to_dec('1', 16) == 1


def test_convert__to_dst():
    assert convert_to_dst(1202, 16) == '4B2'


def test_validate_num_base():
    assert validate_num_base('4B2', 16) is True
    assert validate_num_base('4T2', 17) is False


def test_build_base():
    assert build_base(2) == ['0', '1']
